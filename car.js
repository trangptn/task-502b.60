import {Vehice} from './vehice.js';
class Car extends Vehice{
    _vId;
    _modelName;

    constructor( paramBrand, paramYear,paramVId, paramName)
    {
        super(paramBrand,paramYear);
        this._vId=paramVId;
        this._modelName=paramName;
    }

    getHonk()
    {
        return this._vId +"-" + this._modelName;
    }
}
export {Car};
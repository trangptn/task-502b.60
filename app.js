import  { Vehice} from "./vehice.js";
import { Car } from "./car.js";
import { Motorbike } from "./motorbike.js";

//Task 1
console.log("Sub Task 1");
const vehice1= new Vehice("MWC",2018);
const vehice2= new Vehice("BBC", 2017);
console.log(vehice1);
console.log(vehice2);
console.log(vehice1.getPrint());
console.log(vehice2.getPrint());
console.log(vehice1 instanceof Vehice);
console.log(vehice2 instanceof Car);

//Task 2
console.log("Sub Task 2");
const car1= new Car("MWC",2018,"M001","Xe o to");
const car2= new Car("BBC", 2017,"B002", "Xe moi");
console.log(car1);
console.log(car2);
console.log(car1.getHonk());
console.log(car2.getHonk());
console.log(car1 instanceof Vehice);
console.log(car2 instanceof Car);

//Task 3
console.log("Sub Task 3");
const moto1= new Motorbike("MWC",2018,"M001","Xe mo to 1");
const moto2= new Motorbike("BBC", 2017,"B002", "Xe mo to 2");
console.log(moto1);
console.log(moto2);
console.log(moto1.getHonk());
console.log(moto2.getHonk());
console.log(moto1 instanceof Vehice);
console.log(moto2 instanceof Car);
console.log(moto2 instanceof Motorbike);
class Vehice{
    _brand;
    _yearManufactured;

    constructor(paramBrand,paramYear)
    {
        this._brand=paramBrand;
        this._yearManufactured=paramYear;
    }

    getPrint()
    {
        return this._brand +"-" + this._yearManufactured;
    }
}
export {Vehice}